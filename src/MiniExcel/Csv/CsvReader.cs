﻿using MiniExcelLibs.Utils;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Text.RegularExpressions;

namespace MiniExcelLibs.Csv
{
    internal class CsvReader : IExcelReader
    {
        private Stream _stream;
        public CsvReader(Stream stream)
        {
            this._stream = stream;
        }
        public IEnumerable<IDictionary<string, object>> Query(bool useHeaderRow, string sheetName, string startCell, IConfiguration configuration)
        {
            if (startCell != "A1")
                throw new NotImplementedException("CSV not Implement startCell");
            var cf = configuration == null ? CsvConfiguration.DefaultConfiguration : (CsvConfiguration)configuration;

            using (var reader = cf.GetStreamReaderFunc(_stream))
            {
                var row = string.Empty;
                string[] read;
                var firstRow = true;
                Dictionary<int, string> headRows = new Dictionary<int, string>();
                while ((row = reader.ReadLine()) != null)
                {
                    read = Split(cf, row);

                    //header
                    if (useHeaderRow)
                    {
                        if (firstRow)
                        {
                            firstRow = false;
                            for (int i = 0; i <= read.Length - 1; i++)
                                headRows.Add(i, read[i]);
                            continue;
                        }

                        var cell = Helpers.GetEmptyExpandoObject(headRows);
                        for (int i = 0; i <= read.Length - 1; i++)
                            cell[headRows[i]] = read[i];

                        yield return cell;
                        continue;
                    }


                    //body
                    {
                        var cell = Helpers.GetEmptyExpandoObject(read.Length - 1,0);
                        for (int i = 0; i <= read.Length - 1; i++)
                            cell[Helpers.GetAlphabetColumnName(i)] = read[i];
                        yield return cell;
                    }
                }
            }
        }

        public IEnumerable<T> Query<T>(string sheetName, string startCell, IConfiguration configuration) where T : class, new()
        {
            var cf = configuration == null ? CsvConfiguration.DefaultConfiguration : (CsvConfiguration)configuration;

            var type = typeof(T);

            Dictionary<int, PropertyInfo> idxProps = new Dictionary<int, PropertyInfo>();
            using (var reader = cf.GetStreamReaderFunc(_stream))
            {
                var row = string.Empty;
                string[] read;

                //header
                {
                    row = reader.ReadLine();
                    read = Split(cf, row);

                    var props = Helpers.GetExcelCustomPropertyInfos(type, read);
                    var index = 0;
                    foreach (var v in read)
                    {
                        var p = props.SingleOrDefault(w => w.ExcelColumnName == v);
                        if (p != null)
                            idxProps.Add(index, p.Property);
                        index++;
                    }
                }
                {
                    while ((row = reader.ReadLine()) != null)
                    {
                        read = Split(cf, row);

                        //body
                        {
                            var cell = new T();
                            foreach (var p in idxProps)
                            {
                                if (p.Value.PropertyType.IsEnum)
                                {
                                    var newV = Enum.Parse(p.Value.PropertyType, read[p.Key], true);
                                    p.Value.SetValue(cell, newV);
                                }
                                else
                                {
                                    p.Value.SetValue(cell, read[p.Key]);
                                }
                            }

                            yield return cell;
                        }
                    }

                }
            }
        }

        private static string[] Split(CsvConfiguration cf, string row)
        {
            return Regex.Split(row, $"[\t{cf.Seperator}](?=(?:[^\"]|\"[^\"]*\")*$)")
                .Select(s => Regex.Replace(s.Replace("\"\"", "\""), "^\"|\"$", "")).ToArray();
            //this code from S.O : https://stackoverflow.com/a/11365961/9131476
        }
    }
}
